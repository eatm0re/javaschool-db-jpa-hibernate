create table USER (
  USER_ID INT primary key not null auto_increment,
  FIRST_NAME VARCHAR(45) NOT NULL,
  LAST_NAME VARCHAR(45) NOT NULL,
  BIRTHDAY DATE
);

insert into USER (FIRST_NAME, LAST_NAME, BIRTHDAY)
values ('Evgeniy', 'Dubovitskiy',   STR_TO_DATE('28-03-1995', '%d-%m-%Y')),
       ('Alisa', 'Popova',          STR_TO_DATE('26-04-1998', '%d-%m-%Y')),
       ('Vasiliy', 'Pupkin',        STR_TO_DATE('01-05-1994', '%d-%m-%Y')),
       ('Ivan', 'Ivanov',           STR_TO_DATE('01-01-2000', '%d-%m-%Y')),
       ('Aleksandr', 'Aleksandrov', STR_TO_DATE('13-03-1980', '%d-%m-%Y')),
       ('Alexey', 'Shipilev',       STR_TO_DATE('26-02-1985', '%d-%m-%Y')),
       ('Aleksandr', 'Kshenin',     STR_TO_DATE('03-10-1997', '%d-%m-%Y')),
       ('Alevtina', 'Shaykina',     STR_TO_DATE('31-12-1998', '%d-%m-%Y')),
       ('Nikolay', 'Ivanov',        STR_TO_DATE('22-09-1989', '%d-%m-%Y')),
       ('Viacheslav', 'Kruglov',    STR_TO_DATE('28-02-1987', '%d-%m-%Y'));

select * from USER;


